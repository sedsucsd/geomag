# Geomagnetic Reference Field Simulation

[Download the latest IGRF model here](https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html).

## Usage:
1. Download the correction version for Windows/Linux
2. On windows: Open geomag.exe or execute from the command line `geomag70.exe IGRF13.COF f in.txt out.txt`
3. Recommended to open the exe application file to see all the prompts to decide what to put in the `in.txt` file.
4. Result is generated in the `out.txt` file if runing from command line.
5. `geomag.exe IGRF13.COF date coord alt lat lon` 

Date and location Formats:
   Date: xxxx.xxx for decimal  (1947.32)
         YYYY,MM,DD for year, month, day  (1947,10,13)
         or start_date-end_date (1943.21-1966.11)
         or start_date-end_date-step (1943.21-1966.11-1.2)
   Coord: D - Geodetic (WGS84 latitude and altitude above mean sea level)
          C - Geocentric (spherical, altitude relative to Earth's center)
   Altitude: Kxxxxxx.xxx for kilometers  (K1000.13)
             Mxxxxxx.xxx for meters  (m1389.24)
             Fxxxxxx.xxx for feet  (F192133.73)
   Lat/Lon: xxx.xxx for decimal  (-76.53)
            ddd,mm,ss for degrees, minutes, seconds (-60,23,22)
            (Lat and Lon must be specified in the same format,
             for ddd,mm,ss format, two commas each are required
              and decimals of arc-seconds are ignored)